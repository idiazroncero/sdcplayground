/** @type { import('@storybook/server-webpack5').StorybookConfig } */
const config = {
  stories: [
    '../web/themes/custom/**/*.mdx',
    '../web/themes/custom/**/*.stories.@(json|yml)',
    '../web/modules/**/*.mdx',
    '../web/modules/**/*.stories.@(json|yml)',
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    '@lullabot/storybook-drupal-addon',
    '@storybook/addon-a11y'
  ],
  framework: {
    name: "@storybook/server-webpack5",
    options: {},
  },
  docs: {
    autodocs: "tag",
  },
};
export default config;
