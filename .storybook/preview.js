/** @type { import('@storybook/server').Preview } */
const preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
    server: {
      // Replace this with your Drupal site URL, or an environment variable.
      url: 'http://sdcplayground.ddev.site/',
    },
    globals: {
      drupalTheme: 'sdc_test',
      supportedDrupalThemes: {
        sdc_test: {title: 'SDC Test'},
        radix: {title: 'Radix'},
        olivero: {title: 'Olivero'},
      },
    }
  },
};

export default preview;
