# SDC Playground.

This is a clean Drupal 10.1.x install (with DDEV, optionally) used to test
SDC (SIngle Directory Components).

It will be part of the upcoming dissertation on DrupalCamp 2023 Spain, see
[this link for more info](https://2023.drupalcamp.es/es/el-programa)

## The recipe

For this demonstration, and in the spirit of keeping Drupal as vanilla as
possible, we will use only the following core and contrib modules:

- Core:
  - SDC

- Contrib
  - Devel
  - Admin toolbar
  - SDC Story Generator
  - SDC Display
  - CL Devel (SDC Compatible)
  - CL Server (SDC Compatible)
  - Pokemon Card
  - No markup

We will uninstall the following modules due to a bug causing problems with
rendering:

- Contextual

Plus, a custom (modified) version of Radix Theme will be used. This is
currently stored [here](https://gitlab.com/idiazroncero/radix-sdc)

## How to install

### Without site install

- Install Drupal
- Enable the following modules: `drush en -y sdc devel`
- Enable the following themes: `drush then -y radix sdc_test`
- Set `sdc_test` as the default public theme.

### Compile the theme and components CSS

We are assuming yarn for NPM packages and NVM for node version management, use
your preferred tools here.

```sh
cd web/themes/custom/sdc_test/
nvm use
npm install
cp config/proxy.example.js config/proxy.js
npm run dev
```

You can use `npm run watch` to start a file watcher.

When

### How to create a copy of Radix-SDC

We ship an already created theme (sdc_test) but you can use the new starterkit
to create a copy of the theme (without runtime dependencies) anytime.

`php web/core/scripts/drupal generate-theme --starterkit radix THEME_NAME --path themes/custom`
