# Breadcrumb

Indicate the current page’s location within a navigational hierarchy that automatically adds separators via CSS.

The breadcrumb component expects `breadcrumbs` to be an array of items following this schema. Note that `url` is optional and `text` is required.

```js
[
  {
    url: string,
    text: string
  },
  {
    text: string
  }
]
```

As of Bootstrap 5.2.x, you can use CSS variables to override defaults at anytime:

```css
--#{$prefix}breadcrumb-padding-x: #{$breadcrumb-padding-x};
--#{$prefix}breadcrumb-padding-y: #{$breadcrumb-padding-y};
--#{$prefix}breadcrumb-margin-bottom: #{$breadcrumb-margin-bottom};
@include rfs($breadcrumb-font-size, --#{$prefix}breadcrumb-font-size);
--#{$prefix}breadcrumb-bg: #{$breadcrumb-bg};
--#{$prefix}breadcrumb-border-radius: #{$breadcrumb-border-radius};
--#{$prefix}breadcrumb-divider-color: #{$breadcrumb-divider-color};
--#{$prefix}breadcrumb-item-padding-x: #{$breadcrumb-item-padding-x};
--#{$prefix}breadcrumb-item-active-color: #{$breadcrumb-active-color};
```

Read the original [Bootstrap 5 documentation](https://getbootstrap.com/docs/5.2/components/breadcrumb/) for more information.
