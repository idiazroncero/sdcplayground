# Cards

Bootstrap’s cards provide a flexible and extensible content container with multiple variants and options.

Radix implementation of Bootstrap Cards don't provide all of the possible combinations that appear on Bootstrap 5 Docs. It doesn't make sense, as Cards are more a "wrapper" component - an [organism](https://bradfrost.com/blog/post/atomic-web-design/#organisms), in Atomic design lingo.

Radix Cards only expose props that are specific to the Bootstrap 5 card component, such as `title` or `header_title`. We can identify them because they use semantic classes that start with `.card-`, like `.card-title` or `.card-footer`.

On its minimal expression, a Bootstrap 5 Card is just any HTML wrapped into a `<div class="card">`:

```html
<div class="card">
  <p>This is a minimalistic Bootstrap 5 card!</p>
</div>
```

By default, Radix Cards are ready to render props such as `footer_title`, but you can take full control of any of the three available regions or slots:

```php
{% embed "radix:card" %}

  {% block header %}
    Take full control of the header
  {% endblock %}

  {% block body %}
    Take full control of the body
  {% endblock %}

  {% block footer %}
    Take full control of the footer
  {% endblock %}

{% endembed %}
```

## Caveats

The flexibility of Radix Cards come at a cost. Please consider the following:

- Some `.card-*` classes need to be implemented manually. For example, `.card-img-top` for images on the `header` slot.
- When taking control of a slot, you will need to implement some needed wrappers. For example, when you take control of the `body` slot you will need to wrap your content inside a `<div class="card-body'>` in order to keep the original padding.

## CSS Variables

As of Bootstrap 5.2.x, you can use CSS variables to override buttons anytime:

```css
--#{$prefix}card-spacer-y: #{$card-spacer-y};
--#{$prefix}card-spacer-x: #{$card-spacer-x};
--#{$prefix}card-title-spacer-y: #{$card-title-spacer-y};
--#{$prefix}card-border-width: #{$card-border-width};
--#{$prefix}card-border-color: #{$card-border-color};
--#{$prefix}card-border-radius: #{$card-border-radius};
--#{$prefix}card-box-shadow: #{$card-box-shadow};
--#{$prefix}card-inner-border-radius: #{$card-inner-border-radius};
--#{$prefix}card-cap-padding-y: #{$card-cap-padding-y};
--#{$prefix}card-cap-padding-x: #{$card-cap-padding-x};
--#{$prefix}card-cap-bg: #{$card-cap-bg};
--#{$prefix}card-cap-color: #{$card-cap-color};
--#{$prefix}card-height: #{$card-height};
--#{$prefix}card-color: #{$card-color};
--#{$prefix}card-bg: #{$card-bg};
--#{$prefix}card-img-overlay-padding: #{$card-img-overlay-padding};
--#{$prefix}card-group-margin: #{$card-group-margin};
```

Read the original [Bootstrap 5 documentation](https://getbootstrap.com/docs/5.2/components/card) for more information.
