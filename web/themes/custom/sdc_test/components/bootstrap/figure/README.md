# Figure

Documentation and examples for displaying related images and text with the figure component in Bootstrap.

Anytime you need to display a piece of content—like an image with an optional caption, consider using a `<figure>`.

Read the original [Bootstrap 5 documentation](https://getbootstrap.com/docs/5.2/content/figures/) for more information.

## Where is the CSS?

Figures on Bootstrap 5 form part of a broader file dedicated to
typographic components: `bootstrap/scss/images`. This is why this component don't
feature it's own SCSS / CSS file and is instead loaded globally.

Any overrides for this component should take control of the entire
`_images.scss` file (or use SCSS variables).
