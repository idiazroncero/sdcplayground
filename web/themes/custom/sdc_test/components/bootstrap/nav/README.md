# Navbar

Documentation and examples for displaying related images and text with the figure component in Bootstrap.

Anytime you need to display a piece of content—like an image with an optional caption, consider using a `<figure>`.

Read the original [Bootstrap 5 documentation](https://getbootstrap.com/docs/5.2/content/figures/) for more information.
