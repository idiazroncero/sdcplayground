<?php

namespace Drupal\radix;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Theme\StarterKitInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;



final class StarterKit implements StarterKitInterface {

  /**
   * {@inheritdoc}
   */
  public static function postProcess(string $working_dir, string $machine_name, string $theme_name): void {
    $info_file = "$working_dir/$machine_name.info.yml";
    $fs = new Filesystem();
    $finder = new Finder();
    $info = Yaml::decode(file_get_contents($info_file));
    unset($info['hidden']);
    unset($info['starterkit']);

    // We need to handle the new theme name for interface translations as Drupal
    // core won't do it.
    $info['interface translation project'] = $theme_name;
    $translation_pattern = str_replace('radix', $theme_name, $info['interface translation server pattern']);
    $info['interface translation server pattern'] = $translation_pattern;
    file_put_contents($info_file, Yaml::encode($info));

    // Rename all SDC references on twig templates
    $templates = array_keys(iterator_to_array($finder->in($working_dir)->files()->name(['*.twig'])));
    foreach ($templates as $fileName) {
      $fs->dumpFile(
        $fileName,
        strtr(file_get_contents($fileName), [
          'radix:' => "$theme_name:"
        ])
      );
    }

    // Rename all Stories
    $templates = array_keys(iterator_to_array($finder->in("$working_dir/components")->files()->name(['*.stories.yml'])));
    foreach ($templates as $fileName) {
      $fs->dumpFile(
        $fileName,
        strtr(file_get_contents($fileName), [
          'title: Radix/' => "title: $theme_name/"
        ])
      );
    }
  }
}

